# -*- coding: utf-8 -*-
from django.forms import ModelForm
from django.forms.widgets import Select
from coin.offers.models import Offer
from coin.configuration.models import Configuration


class OfferAdminForm(ModelForm):
    class Meta:
        model = Offer
        widgets = {
            'configuration_type': Select(choices=(('','---------'),) + Configuration.get_configurations_choices_list())
        }
        exclude = ('', )