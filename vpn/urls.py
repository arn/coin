# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import VPNView, VPNGeneratePasswordView, get_graph

urlpatterns = patterns(
    '',
    # This is part of the generic configuration interface (the "name" is
    # the same as the "backend_name" of the model).
    url(r'^(?P<pk>\d+)$', VPNView.as_view(template_name="vpn/vpn.html"), name="details"),
    url(r'^password/(?P<pk>\d+)$', VPNGeneratePasswordView.as_view(template_name="vpn/fragments/password.html"), name="generate_password"),
    url(r'^graph/(?P<vpn_id>[0-9]+)/(?P<period>[a-z]+)$', get_graph, name="get_graph"),
    url(r'^graph/(?P<vpn_id>[0-9]+)$', get_graph, name="get_graph"),
)
