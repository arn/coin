Gestion des abonnements aux listes mail
=======================================

Coin offre un module optionnel pour que les adhérent·e·s puissent
s'abonner/désabonner aux listes mail de l'asso. de manière autonome (listes de
discussions et/ou de diffusion).

Coin n'est pas lui-même un serveur de listes mail. Il se contente de
s'interfacer avec ce dernier pour lui pousser des liste d'inscrit·e·s.

Il existe une interface membre pour auto-gérer ses propres abonnements, et une
interface admin pour gérer l'ensemble des abonnements lorsqu'on possède les
droits admin dans coin.

![vue adhérent](../_img/user-maillists.png)
![vue admin](../_img/admin-maillists.png)

Vues adhérent·e et admin


Fonctionnement
--------------

Coin stocke les abonnements des membres aux listes dans sa base de données, en
utilisant l'adresse mail de la fiche membre.

Il synchronise ensuite ces listes vers le serveur de listes mail en appelant
une commande pour chaque liste gérée. Cette commande est configurable, et
reçoit sur son entrée standard la liste des emails inscrits (une adresse mail
par ligne).

Si le serveur de base données est sur une machine différente du serveur Coin,
il est possible d'utiliser la commande SSH qui appelle un script distant.


La synchronisation sera faite :

- À l'abonnement/désabonnement à une liste via l'interface admin de Coin
  (section « Listes mail ») ;
- à l'abonnement/désabonnement à une liste via l'interface membre de Coin ;
- au changement d'adresse mail d'un·e adhérent·e ;
- à la suppression d'un·e adhérent·e.

Avertissements
---------------

- En l'état, tout membre avec des identifiants Coin aura pouvoir d'auto-gérer
son inscription à toute liste gérée par coin. Il n'est cependant pas
obligatoire donner à Coin la main sur les abonnements à toutes les listes d'un
serveur de listes.

- Il est préférable que les listes mail gérées par Coin le soient totalement. Si
des inscriptions sont faites par d'autres moyens (mail, interface du serveur de
liste de discussions… etc), ces modifications risquent d'être écrasées par la
gestion d'abonnements de Coin.

- La commande de synchro est lancée à chaque abonnement/désabonnement. Il y a
  un outil d'import « en masse » : [import_mailling_list](#méthode-b-importer-des-abonnements-en-masse).

Mise en place
-------------


### 1. Activer l'app *maillist*

Il faut activer l'app *maillists*, cela
fonctionne
[comme les autres apps optionelles](../README.md#using-optional-apps). Il faut
ensuite lancer la commande suivante pour mettre à jour la base de données :

    ./manage.py migrate


### 2. Configurer la synchronisation.


Il y a un paramètre obligatoire à renseigner dans votre fichier.
*settings_local.py* : `MAILLIST_SYNC_COMMAND`.

Il s'agit de la commande à lancer pour « pousser » la liste d'emails inscrits à
une liste mail donnée. 

Vous pouvez utiliser des variables dans la commande à
lancer :

- `{short_name}` : l'identifiant court de la liste (ex: *discussion*)
- `{email}` : l'email de la liste (ex: *discussion@example.com*)

Si par exemple votre serveur de listes attend un bête fichier texte
avec une adresse par ligne dans */etc/lists/nomdelaliste* :


    MAILLIST_SYNC_COMMAND = 'tee /etc/lists/{short_name}'
    
Si le ce même serveur se situe sur un autre serveur, on pourra utiliser par
exemple :

    MAILLIST_SYNC_COMMAND = "ssh coin@mail.example.com 'tee /etc/lists/{short_name}'"

Des cas d'usages plus complexes (ex: inscription via une API HTTP) sont
envisageables en recourant à un petit script sur mesure.


### 3. Ajouter des listes

Deux méthodes, selon que vous voulez initialiser la liste avec une vide ou
pré-remplie avec une liste d'abonnés.

#### Méthode A : créer une liste vide

Se rendre dans l'admin de coin et dans la nouvelle catégorie « Listes mail »,
renseigner les listes mail que l'on souhaite voir gérées par Coin.

#### Méthode B : importer des abonnements « en masse »

Pour créer une liste et faire un import initial de tou·te·s ses abonné·e·s d'un
coup, vous pouvez utiliser la commande `./manage.py import_mailling_list` qui
permet de créer une liste à partir de son adresse, son nom et d'un fichier
texte contenant les adresses à abonner (qui doivent correspondre à des membres
renseignés dans coin).

Pour plus d'infos : `./manage.py import_mailling_list --help`

*NB : Il vous faudra ensuite aller renseigner, via l'interface d'admin de coin,
la description complète de la liste (celle que verront les membres).*
